jQuery(function() {
  initCarousel();
});

function initCarousel() {
  var owlSlider = $(".owl-carousel");
  owlSlider.owlCarousel({
    items: 1,
    dots: true,
    smartSpeed: 1000,
    fluidSpeed: 1500,
    onRefreshed: my_onRefreshed
  }).on('change.owl.carousel', function(e) {
    setTimeout(function(){
      jQuery('.owl-item').removeClass('next_slide');
      jQuery('.owl-item').removeClass('prev_slide');
      jQuery('.owl-item.active').next('.owl-item').addClass('next_slide');
      jQuery('.owl-item.active').prev('.owl-item').addClass('prev_slide');
    }, 1000);
  });

  function my_onRefreshed () {
    return jQuery('.owl-item.active').next('.owl-item').addClass('next_slide');
  }
}